<?php

namespace WowzaStreamingEngine\Libraries;

class Base extends AbstractModules
{
    public function modules()
    {
        return [
            'name'        => 'base',
            'description' => 'Base',
            'class'       => 'com.wowza.wms.module.ModuleCore',
        ];
    }

    public function advancedSettings()
    {
        return [];
    }
}