<?php

namespace WowzaStreamingEngine\Libraries;

abstract class AbstractModules implements ModulesInterface
{
    public function __construct(Array $data = [])
    {
        if (count($data))
            foreach ($data as $key => $value) {
                $this->$key = $value;
            }
    }
}