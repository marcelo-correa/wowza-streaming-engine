<?php

namespace WowzaStreamingEngine\Libraries;

class ModuleCoreSecurity extends AbstractModules
{
    public function modules()
    {
        return [
            'name'        => 'ModuleCoreSecurity',
            'description' => 'Core Security Module for Applications',
            'class'       => 'com.wowza.wms.security.ModuleCoreSecurity',
        ];
    }

    public function advancedSettings()
    {
        return [];
    }
}