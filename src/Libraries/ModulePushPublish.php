<?php

namespace WowzaStreamingEngine\Libraries;

/**
 * Class ModulePushPublish
 * @package WowzaStreamingEngine\Libraries
 * @see https://www.wowza.com/docs/how-to-push-streams-to-cdns-and-other-services-push-publishing
 */
class ModulePushPublish extends AbstractModules
{
    public function modules()
    {
        return [
            'name'        => 'ModulePushPublish',
            'description' => 'ModulePushPublish',
            'class'       => 'com.wowza.wms.pushpublish.module.ModulePushPublish',
        ];
    }

    public function advancedSettings()
    {
        return [
            /**
             * Define o caminho para o arquivo de mapeamento de publicação.
             * Pode ser feito de aplicativo específico usando a notação de variável do aplicativo Wowza.
             * Esta propriedade se aplica a todos os perfis de publicação push.
             */
            [
                'enabled' => true,
                'name'    => "pushPublishMapPath",
                'value'   => $this->pushPublishMapPath ?? '${com.wowza.wms.context.VHostConfigHome}/content/${com.wowza.wms.context.Application}/PushPublishMap.txt',
                'type'    => "String",
                'section' => "/Root/Application",
            ],
        ];
    }
}