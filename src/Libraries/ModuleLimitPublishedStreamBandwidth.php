<?php

namespace WowzaStreamingEngine\Libraries;

/**
 * Class ModuleLimitPublishedStreamBandwidth
 * @package WowzaStreamingEngine\Libraries
 * @see https://www.wowza.com/docs/how-to-monitor-bandwidth-of-published-streams-modulelimitpublishedstreambandwidth
 */
class ModuleLimitPublishedStreamBandwidth extends AbstractModules
{
    public function modules()
    {
        /**
         * O módulo ModuleLimitPublishedStreamBandwidth para o software de servidor de mídia Wowza Streaming Engine ™
         * permite que você desconecte automaticamente fontes RTMP que excedam um limite de largura de banda definido.
         */
        return [
            'name'        => 'ModuleLimitPublishedStreamBandwidth',
            'description' => 'Limite de módulo para largura de banda de fluxo publicada',
            'class'       => 'com.wowza.wms.plugin.collection.module.ModuleLimitPublishedStreamBandwidth',
        ];
    }

    public function advancedSettings()
    {
        return [
            /**
             * Taxa de bits máxima, em kilobits por segundo (Kbps), permitida para qualquer editor.
             * A configuração para 0 desativa a verificação de largura de banda.
             * (padrão: 800)
             */
            [
                'enabled' => true,
                'name'    => "limitPublishedStreamBandwidthMaxBitrate",
                'value'   => $this->limitPublishedStreamBandwidthMaxBitrate ?? 800,
                'type'    => "Integer",
                'section' => "/Root/Application",
            ],
            /**
             * Ativa ou desativa o registro extra.
             * (padrão: false)
             */
            [
                'enabled' => true,
                'name'    => "limitPublishedStreamBandwidthDebugLog",
                'value'   => $this->limitPublishedStreamBandwidthDebugLog ? 'true' : 'false',
                'type'    => "Integer",
                'section' => "/Root/Application",
            ],
        ];
    }
}