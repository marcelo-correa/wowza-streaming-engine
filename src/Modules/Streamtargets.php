<?php

namespace WowzaStreamingEngine\Modules;

use WowzaStreamingEngine\Config\Config;

class Streamtargets extends AbstractModule
{
    protected $module         = 'streamtargets';
    protected $allowedMethods = ['GET', 'POST', 'PUT', 'DELETE'];

    public function __construct()
    {
        $this->prefixUrl = Config::$version . "/servers/" . Config::$serverName . "/vhosts/" . Config::$vhostName . "/applications/__appName__/pushpublish/mapentries";
    }

    /**
     * @return \WowzaStreamingEngine\Response\Response|string
     * @throws \Exception
     */
    public function fetchAll(Array $data)
    {
        if (!isset($data[0]))
            throw new \Exception("Param name app not found.");

        $this->prefixUrl = str_replace('__appName__', $data[0], $this->prefixUrl);

        try {
            return $this->exec('GET');
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $appName
     * @return \WowzaStreamingEngine\Response\Response|string
     * @throws \Exception
     */
    public function find(Array $data)
    {
        $this->prefixUrl = str_replace('__appName__', $data[0], $this->prefixUrl);

        try {
            return $this->exec('GET', $data[1]["entryName"]);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param $body
     * @return \WowzaStreamingEngine\Response\Response|string
     * @throws \Exception
     */
    public function create(Array $data)
    {
        $this->prefixUrl = str_replace('__appName__', $data[0], $this->prefixUrl);

        try {
            return $this->exec('POST', null, $data[1]);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Editar um StreamTarget
     *
     * @param String $appName
     * @param array $body
     * @return \WowzaStreamingEngine\Response\Response|string
     * @throws \Exception
     */
    public function edit(Array $data)
    {
        $this->prefixUrl = str_replace('__appName__', $data[0], $this->prefixUrl);

        if (!isset($data[1]["entryName"]))
            throw new \Exception("Target name not found");
        try {
            return $this->exec('PUT', $data[1]["entryName"], $data[1]);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Deletar um StreamTarget
     *
     * @param $appName
     * @return \WowzaStreamingEngine\Response\Response|string
     * @throws \Exception
     */
    public function delete(Array $data)
    {
        $this->prefixUrl = str_replace('__appName__', $data[0], $this->prefixUrl);

        try {
            return $this->exec('DELETE', $data[1]);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}