<?php

namespace WowzaStreamingEngine\Libraries;

class Logging extends AbstractModules
{
    public function modules()
    {
        return [
            'name'        => 'logging',
            'description' => 'Client Logging',
            'class'       => 'com.wowza.wms.module.ModuleClientLogging',
        ];
    }

    public function advancedSettings()
    {
        return [];
    }
}