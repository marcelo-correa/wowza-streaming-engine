<?php

namespace WowzaStreamingEngine\Libraries;

class Flvplayback extends AbstractModules
{
    public function modules()
    {
        return [
            'name'        => 'flvplayback',
            'description' => 'FLVPlayback',
            'class'       => 'com.wowza.wms.module.ModuleFLVPlayback',
        ];
    }

    public function advancedSettings()
    {
        return [];
    }
}