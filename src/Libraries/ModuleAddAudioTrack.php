<?php

namespace WowzaStreamingEngine\Libraries;

/**
 * Adicione uma faixa de áudio a um stream somente de vídeo usando o módulo AddAudioTrack do Wowza Streaming Engine
 *
 * Class ModuleAddAudioTrack
 * @package WowzaStreamingEngine\Libraries
 * @see https://www.wowza.com/docs/how-to-add-an-audio-track-to-a-video-only-stream-moduleaddaudiotrack
 */
class ModuleAddAudioTrack extends AbstractModules
{
    public function modules()
    {
        return [
            'name'        => 'ModuleAddAudioTrack',
            'description' => 'Adiciona áudio a um fluxo somente de vídeo.',
            'class'       => 'com.wowza.wms.plugin.AddAudioTrack',
        ];
    }

    public function advancedSettings()
    {
        return [
            /**
             * Nome do arquivo que contém a faixa de áudio. O mesmo arquivo é usado para todos os fluxos. (padrão: mp4:sample.mp4 )
             */
            [
                'enabled' => true,
                'name'    => "addAudioTrackAudioSourceFilename",
                'value'   => $this->addAudioTrackAudioSourceFilename,
                'type'    => "String",
                'section' => "/Root/Application",
            ],
        ];
    }
}