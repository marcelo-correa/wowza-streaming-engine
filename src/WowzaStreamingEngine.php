<?php

namespace WowzaStreamingEngine;

use WowzaStreamingEngine\Config\Config;
use WowzaStreamingEngine\Response\Response;

/**
 * @method get($module, ...$args)
 * @method post(callable $callable, $module, ...$args)
 * @method put($module, ...$args)
 * @method delete($module, ...$args)
 *
 * Applications
 * @method fetchAllApplications(callable $callback)
 * @method findApplications(callable $callback, String $appName)
 * @method createApplications(callable $callback, Array $options)
 * @method editApplications(callable $callback, String $appName, Array $options)
 * @method deleteApplications(callable $callback, String $appName)
 * @method monitoringCurrentApplications(callable $callback, String $appName)
 * @method actionsApplications(callable $callback, String $appName, String $action)
 * @method securityApplications(callable $callback, String $appName, Array $options)
 * @method addAuthenticationApplications(callable $callback, $appName, Array $options)
 * @method removeAuthenticationApplications(callable $callback, String $appName, String $publisherName)
 *
 * Users
 * @method fetchAllUsers(callable $callback)
 * @method findUsers(callable $callback, String $user)
 * @method createUsers(callable $callback, Array $options)
 * @method deleteUsers(callable $callback, String $user)
 *
 * Streamtargets
 * @method fetchAllStreamtargets(callable $callback, String $appName)
 * @method findStreamtargets(callable $callback, String $appName, Array $options)
 * @method createStreamtargets(callable $callback, String $appName, Array $options)
 * @method editStreamtargets(callable $callback, String $appName, Array $options)
 * @method deleteStreamtargets(callable $callback, String $appName, String $streamtarget)
 */
class WowzaStreamingEngine
{
    public function __construct(Array $options)
    {
        Config::setOptions($options);
    }

    public function __call($name, $arguments)
    {
        return call_user_func_array([$this, 'execute'], [$name, $arguments]);
    }

    /**
     * @param $action
     * @param mixed ...$args
     * @return mixed
     * @throws \Exception
     */
    private function execute($action, ...$args)
    {
        $arr = preg_split('/(?=[A-Z])/', $action);

        if (count($arr) > 1) {

            if (!$args[0][0] instanceof \Closure) {
                throw new \Exception("The first parameter requires a \Closure instance");
            }

            $className = "WowzaStreamingEngine\\Modules\\" . ucfirst(end($arr));
            array_pop($arr);
            $class  = new $className;
            $action = implode('', $arr);

            $callable = $args[0][0];
            unset($args[0][0]);

            $result = $class->$action(array_values($args[0]));
        } else {

            $exp    = explode('_', strtolower($args[0][0]));
            $module = implode('', $exp);

            $options  = !is_callable($args[0][1]) ? $args[0][1] : null;
            $body     = isset($args[0][2]) ? $args[0][2] : null;
            $callable = end($args[0]);

            $className = "WowzaStreamingEngine\\Modules\\" . ucfirst($module);
            $class     = new $className;
            $result    = $class->$action($options, $body);
        }


        if (!$result instanceof Response) {
            throw new \Exception($result);
        }

        return call_user_func($callable, $result);
    }
}