<?php

namespace WowzaStreamingEngine\Libraries;

/**
 * O módulo ModuleLimitStreamViewers para o software de servidor de mídia Wowza Streaming Engine ™
 * permite limitar o número de conexões simultâneas do visualizador a um fluxo.
 * Você pode definir um limite de exibição para um arquivo inteiro ou definir opções para cada fluxo em um aplicativo.
 *
 * Class ModuleLimitStreamViewers
 * @package WowzaStreamingEngine\Libraries
 * @see https://www.wowza.com/docs/how-to-limit-the-number-of-viewers-to-a-stream-modulelimitstreamviewers
 */
class ModuleLimitStreamViewers extends AbstractModules
{
    /**
     * Limitar o número de espectadores de um fluxo (LimitStreamViewers)
     * @return array
     */
    public function modules()
    {
        return [
            'name'        => 'ModuleLimitStreamViewers',
            'description' => 'Limit stream viewers',
            'class'       => 'com.wowza.wms.plugin.collection.module.ModuleLimitStreamViewers',
        ];
    }

    public function advancedSettings()
    {
        return [
            /**
             * Total de conexões para qualquer fluxo. (padrão: 200)
             */
            [
                'enabled' => true,
                'name'    => "limitStreamViewersMaxViewers",
                'value'   => $this->limitStreamViewersMaxViewers ?? 200,
                'type'    => "Integer",
                'section' => "/Root/Application",
            ],
            /**
             * Total de conexões por fluxo. (padrão: não definido)
             */
            [
                'enabled' => true,
                'name'    => "limitStreamViewersByList",
                'value'   => $this->limitStreamViewersByList ?? "myStream=10,myOtherStream=20",
                'type'    => "String",
                'section' => "/Root/Application",
            ],
            /**
             * Registrar conexões no fluxo. (padrão: true)
             */
            [
                'enabled' => true,
                'name'    => "limitStreamViewersLogConnectionCounts",
                'value'   => $this->limitStreamViewersLogConnectionCounts ?? 'true',
                'type'    => "Boolean",
                'section' => "/Root/Application",
            ],
            /**
             * Registrar as conexões rejeitadas no fluxo. (padrão: true)
             */
            [
                'enabled' => true,
                'name'    => "limitStreamViewersLogRejections",
                'value'   => $this->limitStreamViewersLogRejections ?? 'true',
                'type'    => "Boolean",
                'section' => "/Root/Application",
            ],
        ];
    }
}