<?php

namespace WowzaStreamingEngine\Libraries;

/**
 * Class ModuleHotlinkDenial
 * @package WowzaStreamingEngine\Libraries
 */
class ModuleHotlinkDenial extends AbstractModules
{
    public function modules()
    {
        return [
            'name'        => 'ModuleHotlinkDenial',
            'description' => 'Define uma lista de domínios de sites hotlinkable e nega links para outros domínios.',
            'class'       => 'com.wowza.wms.plugin.collection.module.ModuleHotlinkDenial',
        ];
    }

    public function advancedSettings()
    {
        return [
            [
                'enabled' => true,
                'name'    => "hotlinkDomains",
                'value'   => $this->hotlinkDomains ?? "*", // <---- aqui vai os endereços bloqueados
                'type'    => "String",
                'section' => "/Root/Application",
            ],
            [
                'enabled' => true,
                'section' => "/Root/Application",
                'name'    => "hotlinkEncoders",
                'type'    => "String",
                'value'   => $this->hotlinkEncoders ?? "Wirecast",
            ],
            [
                'enabled' => true,
                'section' => "/Root/Application",
                'name'    => "hotlinkLogConnections",
                'type'    => "Boolean",
                'value'   => $this->hotlinkLogConnections ?? 'false',
            ],
            [
                'enabled' => true,
                'section' => "/Root/Application",
                'name'    => "hotlinkLogRejections",
                'type'    => "Boolean",
                'value'   => $this->hotlinkLogRejections ?? 'false',
            ],
        ];
    }
}