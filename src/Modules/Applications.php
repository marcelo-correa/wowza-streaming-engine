<?php

namespace WowzaStreamingEngine\Modules;

use WowzaStreamingEngine\Config\Config;

class Applications extends AbstractModule
{
    protected $module         = 'applications';
    protected $allowedMethods = ['GET', 'POST', 'PUT', 'DELETE'];

    public function __construct()
    {
        $this->prefixUrl = Config::$version . "/servers/" . Config::$serverName . "/vhosts/" . Config::$vhostName . "/{$this->module}";
    }

    /**
     * @return \WowzaStreamingEngine\Response\Response|string
     * @throws \Exception
     */
    public function fetchAll()
    {
        try {
            return $this->exec('GET');
        } catch (\Exception $e) {
            //\Logs::channel("exceptions")->info("L" . __LINE__ . " > " . __CLASS__ . " message `{$e->getMessage()}` - file `{$e->getFile()}` - line `{$e->getLine()}`");
            throw $e;
        }
    }

    /**
     * @param array $data
     * @return \WowzaStreamingEngine\Response\Response|string
     * @throws \Exception
     */
    public function find(Array $data)
    {
        try {
            return $this->exec('GET', $data[0]);
        } catch (\Exception $e) {
            //\Logs::channel("exceptions")->info("L" . __LINE__ . " > " . __CLASS__ . " message `{$e->getMessage()}` - file `{$e->getFile()}` - line `{$e->getLine()}`");
            throw $e;
        }
    }

    /**
     * @param array $data
     * @return \WowzaStreamingEngine\Response\Response|string
     * @throws \Exception
     */
    public function create(Array $data)
    {
        try {
            return $this->exec('POST', null, $data[0]);
        } catch (\Exception $e) {
            //\Logs::channel("exceptions")->info("L" . __LINE__ . " > " . __CLASS__ . " message `{$e->getMessage()}` - file `{$e->getFile()}` - line `{$e->getLine()}`");
            throw $e;
        }
    }

    /**
     * @param array $data
     * @return \WowzaStreamingEngine\Response\Response|string
     * @throws \Exception
     */
    public function edit(Array $data)
    {
        try {
            return $this->exec('PUT', $data[0], $data[1]);
        } catch (\Exception $e) {
            //\Logs::channel("exceptions")->info("L" . __LINE__ . " > " . __CLASS__ . " message `{$e->getMessage()}` - file `{$e->getFile()}` - line `{$e->getLine()}`");
            throw $e;
        }
    }

    /**
     * @param array $data
     * @return \WowzaStreamingEngine\Response\Response|string
     * @throws \Exception
     */
    public function delete(Array $data)
    {
        try {
            return $this->exec('DELETE', $data[0]);
        } catch (\Exception $e) {
            //\Logs::channel("exceptions")->info("L" . __LINE__ . " > " . __CLASS__ . " message `{$e->getMessage()}` - file `{$e->getFile()}` - line `{$e->getLine()}`");
            throw $e;
        }
    }

    /**
     * @param array $data
     * @return \WowzaStreamingEngine\Response\Response|string
     * @throws \Exception
     */
    public function monitoringCurrent(Array $data)
    {
        try {
            return $this->exec('GET', "{$data[0]}/monitoring/current");
        } catch (\Exception $e) {
            //\Logs::channel("exceptions")->info("L" . __LINE__ . " > " . __CLASS__ . " message `{$e->getMessage()}` - file `{$e->getFile()}` - line `{$e->getLine()}`");
            throw $e;
        }
    }

    /**
     * @param array $data
     * @return \WowzaStreamingEngine\Response\Response|string
     * @throws \Exception
     */
    public function actions(Array $data)
    {
        try {
            return $this->exec('PUT', "{$data[0]}/actions/{$data[1]}");
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param array $data
     * @return \WowzaStreamingEngine\Response\Response|string
     * @throws \Exception
     */
    public function security(Array $data)
    {
        try {
            return $this->exec('POST', "{$data[0]}/publishers", $data[1]);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param array $data
     * @return \WowzaStreamingEngine\Response\Response|string
     * @throws \Exception
     */
    public function addAuthentication(Array $data)
    {
        try {
            return $this->exec('POST', "{$data[0]}/publishers", $data[1]);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param array $data
     * @return \WowzaStreamingEngine\Response\Response|string
     * @throws \Exception
     */
    public function removeAuthentication(Array $data)
    {
        try {
            return $this->exec('DELETE', "{$data[0]}/publishers/$data[1]");
        } catch (\Exception $e) {
            throw $e;
        }
    }
}