<?php

namespace WowzaStreamingEngine\Libraries;

/**
 * Class ModuleAnalytics
 * @package WowzaStreamingEngine\Libraries
 * @see https://www.wowza.com/docs/how-to-send-connection-and-stream-statistics-to-google-analytics-analytics
 */
class ModuleAnalytics extends AbstractModules
{
    public function modules()
    {
        return [
            'name'        => 'ModuleAnalytics',
            'description' => 'Envia estatísticas para o Google Analytics.',
            'class'       => 'com.wowza.wms.plugin.Analytics',
        ];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function advancedSettings()
    {
        if (!isset($this->wowzalyticsGACode) || is_null($this->wowzalyticsGACode))
            throw new \Exception("GA Code not found.");

        if (!isset($this->wowzalyticsGADomain) || is_null($this->wowzalyticsGADomain))
            throw new \Exception("GA Domain not found.");

        if (!isset($this->wowzalyticsGAHost) || is_null($this->wowzalyticsGAHost))
            throw new \Exception("GA Host not Host.");

        if (!isset($this->wowzalyticsGAPrefix) || is_null($this->wowzalyticsGAPrefix))
            throw new \Exception("GA Prefix not found.");

        if (!isset($this->wowzalyticsStatsNotificationUrls) || is_null($this->wowzalyticsStatsNotificationUrls))
            throw new \Exception("Stats Notification Urls not found.");

        return [
            /**
             * ID de acompanhamento do domínio (propriedade do website) que está sendo rastreado no Google Analytics.
             * (padrão: não definido)
             */
            [
                'enabled' => true,
                'name'    => "wowzalyticsGACode",
                'value'   => $this->wowzalyticsGACode,
                'type'    => "String",
                'section' => "/Root/Application",
            ],
            /**
             * Nome de domínio (propriedade do site) sendo rastreado. (padrão: não definido)
             */
            [
                'enabled' => true,
                'name'    => "wowzalyticsGADomain",
                'value'   => $this->wowzalyticsGADomain,
                'type'    => "String",
                'section' => "/Root/Application",
            ],
            /**
             * Nome do host sendo rastreado. (padrão: não definido)
             */
            [
                'enabled' => true,
                'name'    => "wowzalyticsGAHost",
                'value'   => $this->wowzalyticsGAHost,
                'type'    => "String",
                'section' => "/Root/Application",
            ],
            /**
             * Prefixo opcional a ser usado. (padrão: não definido)
             */
            [
                'enabled' => true,
                'name'    => "wowzalyticsGAPrefix",
                'value'   => $this->wowzalyticsGAPrefix,
                'type'    => "String",
                'section' => "/Root/Application",
            ],
            /**
             * Lista separada por vírgula opcional de URLs para enviar as estatísticas.
             * Se usadas, essas URLs devem poder aceitar as solicitações. (padrão: não definido)
             */
            [
                'enabled' => true,
                'name'    => "wowzalyticsStatsNotificationUrls",
                'value'   => $this->wowzalyticsStatsNotificationUrls,
                'type'    => "String",
                'section' => "/Root/Application",
            ],
            /**
             * Permite o registro extra de depuração. (padrão: false)
             */
            [
                'enabled' => true,
                'name'    => "wowzalyticsDebug",
                'value'   => $this->wowzalyticsDebug ?? 'false',
                'type'    => "Boolean",
                'section' => "/Root/Application",
            ],

            /**
             * Número de encadeamentos usados ​​para enviar dados. (padrão: 5)
             */
            [
                'enabled' => true,
                'name'    => "wowzalyticsThreadPoolSize",
                'value'   => $this->wowzalyticsThreadPoolSize ?? 5,
                'type'    => "Integer",
                'section' => "/Root/Server",
            ],
            /**
             * Tempo, em milissegundos, para aguardar entre solicitações com falha. (padrão: 1000)
             */
            [
                'enabled' => true,
                'name'    => "wowzalyticsDelayForFailedRequests",
                'value'   => $this->wowzalyticsDelayForFailedRequests ?? 1000,
                'type'    => "Integer",
                'section' => "/Root/Server",
            ],
            /**
             * Número de vezes para repetir uma solicitação com falha antes de desistir. (padrão: 5)
             */
            [
                'enabled' => true,
                'name'    => "wowzalyticsHTTPMaxRetries",
                'value'   => $this->wowzalyticsHTTPMaxRetries ?? 5,
                'type'    => "Integer",
                'section' => "/Root/Server",
            ],
            /**
             * Ao desligar, tempo, em segundos, para aguardar para permitir que todas as solicitações sejam enviadas.
             * (padrão: 5)
             */
            [
                'enabled' => true,
                'name'    => "wowzalyticsThreadPoolTerminationTimeout",
                'value'   => $this->wowzalyticsThreadPoolTerminationTimeout ?? 5,
                'type'    => "Integer",
                'section' => "/Root/Server",
            ],
        ];
    }
}