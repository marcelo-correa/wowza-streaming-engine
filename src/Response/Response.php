<?php

namespace WowzaStreamingEngine\Response;

class Response
{
    private $headers;
    private $body;
    private $url;
    private $status;

    /**
     * Response constructor.
     * @param $headers
     * @param $body
     * @param $url
     * @param $status
     */
    public function __construct($headers, $body, $url, $status)
    {
        $this->headers = $headers;
        $this->body    = $body;
        $this->url     = $url;
        $this->status  = $status;
    }

    /**
     * @return mixed
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    public function getBodyToArray()
    {
        return json_decode($this->body, true);
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    public function getStatus()
    {
        return $this->status;
    }

}