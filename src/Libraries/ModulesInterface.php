<?php

namespace WowzaStreamingEngine\Libraries;

interface ModulesInterface
{
    public function modules();

    public function advancedSettings();
}