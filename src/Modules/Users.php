<?php

namespace WowzaStreamingEngine\Modules;

use WowzaStreamingEngine\Config\Config;

class Users extends AbstractModule
{
    protected $module         = 'users';
    protected $allowedMethods = ['GET', 'POST', 'PUT', 'DELETE'];

    public function __construct()
    {
        $this->prefixUrl = Config::$version . "/servers/" . Config::$serverName . "/{$this->module}";
    }

    /**
     * @return \WowzaStreamingEngine\Response\Response|string
     * @throws \Exception
     */
    public function fetchAll()
    {
        try {
            return $this->exec('GET');
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param array $data
     * @return \WowzaStreamingEngine\Response\Response|string
     * @throws \Exception
     */
    public function find(Array $data)
    {
        try {
            return $this->exec('GET', $data[0]);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param array $data
     * @return \WowzaStreamingEngine\Response\Response|string
     * @throws \Exception
     */
    public function create(Array $data)
    {
        $body = [
            "serverName"  => Config::$serverName,
            "userName"    => $data[0]["userName"],
            "password"    => $data[0]["password"],
            "description" => $data[0]["description"],
            "groups"      => $data[0]["groups"],
        ];

        try {
            return $this->exec('POST', null, $body);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param array $data
     * @return \WowzaStreamingEngine\Response\Response|string
     * @throws \Exception
     */
    public function delete(Array $data)
    {
        try {
            return $this->exec('DELETE', $data[0]);
        } catch (\Exception $e) {
            throw $e;
        }
    }
}