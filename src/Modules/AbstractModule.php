<?php

namespace WowzaStreamingEngine\Modules;

use WowzaStreamingEngine\Request\Request;
use WowzaStreamingEngine\Response\Response;

abstract class AbstractModule
{
    protected $module;
    protected $prefixUrl;
    protected $allowedMethods;

    /**
     * @param $name
     * @param $arguments
     * @return Response|string
     * @throws \Exception
     */
    public function __call($name, $arguments)
    {
        $stringUrl = $arguments[0];
        $body      = is_array($arguments[1]) ? $arguments[1] : [];

        if (in_array($name, ["get", "post", "put", "delete"])) {
            return $this->exec(strtoupper($name), $stringUrl, $body);
        } else {
            throw new \Exception("method not available");
        }
    }

    /**
     * @param $method
     * @param null $stringUrl
     * @param array $body
     * @return Response|string
     * @throws \Exception
     */
    public function exec($method, $stringUrl = null, $body = [])
    {
        if (!in_array($method, $this->allowedMethods)) {
            throw new \Exception("method $method not available");
        }

        $stringUrl = !is_null($stringUrl) ? "/$stringUrl" : null;
        $strUrl    = "{$this->prefixUrl}$stringUrl";

        $request = new Request;

        return $request->exec($strUrl, $method, $body);
    }
}